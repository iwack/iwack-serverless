import os
import json
import boto3

dynamodb = boto3.resource('dynamodb')


def get_athlete_by_id(email):
    
    table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])
    response = table.get_item(
            Key={ "email": email }
        )
    if not response.get('Item'):
        return 'Athlete does not exist'
    
    response = response.get('Item')
    return response

def get(event, context):
    email = (event['pathParameters']['email'])
    response = get_athlete_by_id(email)
    if not response:
        return {
            'statusCode': 404,
            'isBase64Encoded': False,
            "headers": {
                "Access-Control-Allow-Origin": "*"
            },
            'body': json.dumps('Athlete does not exist')
        }

    return {
        'statusCode': 200,
        'isBase64Encoded': False,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        'body': json.dumps(response)
    }