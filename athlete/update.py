import os
import json
import uuid
import boto3

def update_athlete(event, email):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])
    parsed = json.loads(event["body"])
    given_name = parsed["given_name"]
    family_name = parsed["family_name"]
    objectives = parsed["objectives"]
    train_methods = parsed["training"]
    DoB=parsed["DoB"]
    gender=parsed["gender"]
    email=parsed["email"]
    allergies=parsed["allergies"]
    
    table.update_item(
        Key={'email': email},
        UpdateExpression="set #name = :n, gender=:g, allergies = :a, DoB = :d, training_preference= :t, objectives= :o ",
        ExpressionAttributeValues={
            ':n': given_name,
            ':g': gender,
            ':a': allergies,
            ':d': DoB,
            ':t': train_methods,
            ':o': objectives
        },
        ExpressionAttributeNames={
            "#name": "name"
        },
        ReturnValues="UPDATED_NEW"
    )
    
    return 'athlete updated'
    

def lambda_handler(event, context):
    email = (event['pathParameters']['email'])
    response = update_athlete(event, email)
    return {
        "statusCode": 200,
        "isBase64Encoded": False,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        "body": json.dumps(response)
    }
