import os
import json
import uuid
import boto3

dynamodb = boto3.resource('dynamodb')

def create_athlete(event):
    id = str(uuid.uuid4())
    parsed = json.loads(event["body"])
    given_name = parsed["given_name"]
    family_name = parsed["family_name"]
    objectives = parsed["objectives"]
    train_methods = parsed["training"]
    gender=parsed["gender"]
    email=parsed["email"]
    
    table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])

    if not parsed.get('allergies') and not parsed.get('DoB'):
        table.put_item(
            Item = {
                'id': id,
                'email': email,
                'name': given_name + " " + family_name,
                'gender': gender,
                'objectives': objectives,
                'training_preference': train_methods
            }
        )
        return 'athlete added'
        
    elif not parsed.get('allergies'):
        DoB=parsed["DoB"]
        table.put_item(
            Item={
                'email': email,
                'name': given_name,
                'gender': gender,
                'objectives': objectives,
                'training_preference': train_methods,
                'allergies': 'None',
                'DoB': DoB
            },
            ConditionExpression="attribute_exists(id)"
        )
        return 'athlete added'
        
    else:
        allergies=parsed['allergies']
        DoB=parsed["DoB"]
        table.put_item(
            Item = {
                'email': email,
                'name': given_name,
                'gender': gender,
                'objectives': objectives,
                'training_preference': train_methods,
                'allergies': allergies,
                'DoB': DoB
                
            },
            ConditionExpression="attribute_exists(id)"
        )
        return 'athlete added'


def profile(event, context):
    
    response = create_athlete(event)
    
    return {
        "statusCode": 200,
        "isBase64Encoded": False,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        "body": json.dumps(response)
    }
