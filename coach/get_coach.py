import os
import json
import boto3
from boto3.dynamodb.conditions import Key, Attr
import decimal

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if abs(o) % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)
        
def get_coach_by_id(coach_id):
    
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(os.environ['DYNAMODB_COACH'])
    
    response = table.scan(
        FilterExpression=Attr('id').eq(coach_id)
    )
    
    return response['Items']

def lambda_handler(event, context):
    coach_id = (event['pathParameters']['id'])
    response = get_coach_by_id(coach_id)
    if not response:
        return {
            'statusCode': 404,
            'isBase64Encoded': False,
            "headers": {
                "Access-Control-Allow-Origin": "*"
            },
            'body': json.dumps('Coach does not exist')
        }

    return {
        'statusCode': 200,
        'isBase64Encoded': False,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        'body': json.dumps(response, cls=DecimalEncoder)
    }