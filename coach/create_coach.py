import os
import json
import uuid
import boto3

dynamodb = boto3.client('dynamodb')

def create_coach(event):
    id = str(uuid.uuid4())
    parsed = json.loads(event["body"])
    name = parsed["name"]
    DoB=parsed["DoB"]
    gender=parsed["gender"]
    email=parsed["email"]
    description=parsed["description"]
    rating=parsed["rating"]
    img=parsed["img"]


    dynamodb.put_item(
        TableName = os.environ['DYNAMODB_COACH'],
        Item={
            'id': {
                'S': id,
            },
            'name': {
                'S': name,
            },
            'DoB': {
                'S': DoB,
            },
            'gender': {
                'S': gender,
            },
            'email': {
                'S': email,
            },
            'description': {
                'S': description,
            },
            'rating': {
                'S': rating,
            },
            'img': {
                'S': img,
            }

        })
    return 'coach added'

def update_coach(event, email):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(os.environ['DYNAMODB_COACH'])
    parsed = json.loads(event["body"])
    train_methods = parsed["training"]
    DoB=parsed["DoB"]
    description=parsed["description"]
    
    table.update_item(
        Key={'email': email},
        UpdateExpression="set DoB = :d, training_preference = :t, description = :des",
        ExpressionAttributeValues={
            ':d': DoB,
            ':t': train_methods,
            ':des': description
        },
        ReturnValues="UPDATED_NEW"
    )
    
    return 'coach updated'
    


def lambda_handler(event, context):
    emailPath = (event['queryStringParameters']['email'])
    if not emailPath:
        response = create_coach(event)
    else:
        response = update_coach(event, emailPath)
    
    
    return {
        "statusCode": 200,
        "isBase64Encoded": False,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        "body": json.dumps(response)
    }
