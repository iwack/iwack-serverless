import os
import json
import boto3
import decimal

dynamodb = boto3.client('dynamodb')

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if abs(o) % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)

def list_coach():
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(os.environ['DYNAMODB_COACH'])
    response = table.scan()
    return response['Items']

def get_coach_by_email(email):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(os.environ['DYNAMODB_COACH'])
    response = table.get_item(
        Key={
            'email': email
        }
    )
    return response['Item']

def lambda_handler(event, context):
    
    if not event['queryStringParameters']:
        response = list_coach()
    else:
        email = (event['queryStringParameters']['email'])
        response = get_coach_by_email(email)

    return {
        'statusCode': 200,
        'isBase64Encoded': False,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        'body': json.dumps(response, cls=DecimalEncoder)
    }