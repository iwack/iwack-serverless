import json
import boto3
import os

def lambda_handler(event, context):
    email = event['pathParameters']['email']
    body = json.loads(event['body'])
    disponibilities = body['dispos']
    if is_valid(disponibilities):
        return {
            'statusCode': 200,
            "headers": {
                "Access-Control-Allow-Origin": "*"
            },
            'body': json.dumps({
                'dispos': update_dispos(email, disponibilities)
            })
        }
    else:
       return {
            'statusCode': 400,
            'body': json.dumps('invalid disponibilities')
        } 

def is_valid(disponibilities):
    return True

def update_dispos(email, disponibilities):
    
    response = boto3.client('dynamodb').update_item(
        TableName = os.environ['DYNAMODB_COACH'],
        Key = {'email': {'S': email}},
        UpdateExpression=" set disponibilities = :d",
        ExpressionAttributeValues={
            ":d": dynamoDB_format(disponibilities)
        }
    )
    print(response)
    print(str(response['ResponseMetadata']['HTTPStatusCode'] == 200))
    response = boto3.client('dynamodb').get_item(
        TableName = os.environ['DYNAMODB_COACH'],
        Key={'email': {'S': email} },
        ProjectionExpression = 'disponibilities'
    )['Item']
    print(response)
    return dynamo_to_python(response['disponibilities'])

def dynamoDB_format(raw):
    if isinstance(raw, dict):
        return {
            'M': {
                key: dynamoDB_format(value)
                for key, value in raw.items()
            }
        }
    elif isinstance(raw, list):
        return {
            'L': [dynamoDB_format(value) for value in raw]
        }
    elif isinstance(raw, str):
        return {'S': raw}
    elif isinstance(raw, bool):
        return {'BOOL', raw}
    elif isinstance(raw, (int, float)):
        return {'N': str(raw)}
    elif isinstance(raw, bytes):
        return {'B', raw}
    elif raw is None:
        return {'NULL': True}

def dynamo_to_python(raw):
    avaibilities = []
    dict_response = {}
    for item in raw.get('L'):
        dict_response['id'] = item['M'].get('id').get('N')
        dict_response['name'] = item['M'].get('name').get('S')
        dict_response['start'] = item['M'].get('start').get('S')
        dict_response['end'] = item['M'].get('end').get('S')
        avaibilities.append(dict_response)
        
    return json.loads(json.dumps(avaibilities))