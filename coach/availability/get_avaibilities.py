import json
import boto3
import os

def lambda_handler(event, context):
    email = event['pathParameters']['email']
    return {
        'statusCode': 200,
        'isBase64Encoded': False,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        'body': json.dumps({
            'dispos': get_dispos(email)
        })
    }



def get_dispos(email):
    response = boto3.client('dynamodb').get_item(
        TableName = os.environ['DYNAMODB_COACH'],
        Key={'email': {'S': email} },
        ProjectionExpression = 'disponibilities'
    )['Item']
    
    avaibilities = []
    dict_response = {}
    for item in response.get('disponibilities').get('L'):
        dict_response['id'] = int(item.get('M').get('id').get('N'))
        dict_response['name'] = item.get('M').get('name').get('S')
        dict_response['start'] = item.get('M').get('start').get('S')
        dict_response['end'] = item.get('M').get('end').get('S')
        avaibilities.append(dict_response.copy())
        
        
    return json.loads(json.dumps(avaibilities))
