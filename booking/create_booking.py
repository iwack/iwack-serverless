import os
import json
import uuid
import boto3

dynamodb = boto3.client('dynamodb')

def create_booking(event):
    id = str(uuid.uuid4())
    parsed = json.loads(event["body"])
    coach_id = parsed["coach_id"]
    athlete_id = parsed["athlete_id"]
    date = parsed["date"]
    price = parsed["price"]
    course_type = parsed["course_type"]
    meeting = parsed["meeting"]


    dynamodb.put_item(
        TableName = os.environ['DYNAMODB_BOOKING'],
        Item={
            'id': {
                'S': id,
            },
            'coach_id': {
                'S': coach_id,
            },
            'athlete_id': {
                'S': athlete_id,
            },
            'date': {
                'S': date,
            },
            'price': {
                'S': price,
            },
            'course_type': {
                'S': course_type,
            },
            'meeting': {
                'S': meeting,
            },
            'status': {
                'S': "waiting"
            }

        })
    return 'booking added'




def lambda_handler(event, context):
    
    response = create_booking(event)
    
    return {
        "statusCode": 200,
        "isBase64Encoded": False,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        "body": json.dumps(response)
    }
