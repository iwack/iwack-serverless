import json
import boto3
import os
from boto3.dynamodb.conditions import Key, Attr

def create_db(athlete_id, status):
    
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(os.environ['DYNAMODB_BOOKING'])
    
    response = table.scan(
        FilterExpression=Attr('athlete_id').eq(athlete_id) & Attr('status').eq(status)
    )
    
    return response['Items']


def lambda_handler(event, context):
    
    athlete_id = (event['pathParameters']['id'])
    status = (event["queryStringParameters"]["status"])
    response = create_db(athlete_id, status)
    
    return {
        "statusCode": 200,
        "isBase64Encoded": False,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        'body': json.dumps(response)
    }
